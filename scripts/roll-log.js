class SeparateRollLog extends Application {
	constructor(options) {
		super(options);

	    getTemplate('public/modules/separate-roll-log/roll.html');
	    getTemplate('public/modules/separate-roll-log/hidden-roll.html');

	    this._settings = {
	    	timestampFormat: 'dynamic',
	    	hideFormulas: true,
	    	showSidebarRolls: true,
	    	newRollExpanded: true
	    };

	    Hooks.on('createChatMessage', (message, data) => {
	    	if(!message.isRoll) return;
	    	const renderMessage = async function () {
	    		let rollHTML = message.visible ? 
	    			await renderTemplate('public/modules/separate-roll-log/roll.html' , {message: message, isGM: game.user.isGM}) : 
	    			await renderTemplate('public/modules/separate-roll-log/hidden-roll.html' , {message: message, isGM: game.user.isGM});
	    		let $container = $('#separate-roll-log .roll-content')
	    		if(ui.separateRollLog._settings.newRollExpanded){
	    			if($(rollHTML).hasClass('expandable')){
	    				rollHTML = $(rollHTML).addClass('expand');
	    				$container.children('.expand').last().removeClass('expand');
	    			}
	    		}
	    		// Add expand click listener only on DOM .roll elements that have a .dice child to expand out
	    		$container.append($(rollHTML))
	    			.find(`.roll[data-message-id="${message.id}"] .diceresult .dice`)
	    				.parents('.expandable.roll').click(function(){$(this).toggleClass('expand')})
	    				.find('.message-delete').click(async function(ev){
	    					ev.stopPropagation();
	    					let rollItem = $(this).parents('.roll');
	    					let mID = rollItem.attr('data-message-id');
	    					let message = game.messages.get(mID);
	    					let del = await message.delete();
	    					console.log("Deleted from roll log ChatMessage ID:",del);
	    				});
	    		$container.animate({scrollTop: $container.prop('scrollHeight')},100);
	    	};
	    	renderMessage();
	    	
	    });

	    Hooks.on('renderChatMessage', (message, data, html) =>{
	    	if(!message.isRoll) return;
	    	this._applySidebarRollsOne(message, data, html);
	    });

	    Hooks.on('deleteChatMessage', (deletedMessage, options) => {
	    	if(!deletedMessage.isRoll) return;
	    	this._removeDOMEl(deletedMessage.id);
	    	console.log(`Removed ${deletedMessage.id} from roll log due to deleteChatMessage event`);
	    })

	}

	/* -------------------------------------------- */

 	/**
   	* Assign the default options 
   	*/
   	static get defaultOptions() {
   		const options = super.defaultOptions;
   		options.id = "separate-roll-log";
   		options.template = "public/modules/separate-roll-log/separate-roll-log.html";
   		options.width = 500;
        options.height = 350;
        options.resizable = false;
        options.classes = ["separate-roll-log"];
   		options.popOut = false;
   		return options;
   	}

   	get SETTINGS(){
		return {
			timestampFormat: {
				name: 'Timestamp formatting',
				hint: 'Control how timestamps are formatted, or whether they are displayed at all',
				scope: 'client',
				type: String,
				default: 'dynamic',
				choices: {
					dynamic: 'Dynamically show time or date+time',
					short: 'Time only',
					none: 'No timestamps'
				},
				config: true,
				onChange: setting => {
					this._settings.timestampFormat = setting;
					this._setTimestampCSS();
				}
			},
			showSidebarRolls: {
				name: 'Display rolls in sidebar',
				hint: 'Choose whether to display rolls only in roll log or also in sidebar chat log',
				scope: 'client',
				type: Boolean,
				default: false,
				config: true,
				onChange: setting => {
					this._settings.showSidebarRolls = setting; 
					if(setting)
						this._rerenderSidebarRolls();
					else
						this._hideSidebarRolls();
				}
			},
			hideFormulas: {
				name: "Hide roll formulas",
				hint: "Hide roll formulas from players who don't have ownership over associated actor",
				scope: 'world',
				type: Boolean,
				default: false,
				config: true,
				onChange: setting => {
					this._settings.hideFormulas = setting;
					this.close();
					this.render(true);
					if(setting){
						this._hideFormulasFromChatLog();
					}
					else{
						game.messages.entities.filter(m => m.isRoll).forEach(async m => {
						let newHTML = await m.render();
						$(`#sidebar #chat-log [data-message-id="${m.id}"]`).replaceWith(newHTML);
					});
					}
				}
			},
			newRollExpanded: {
				name: 'Expand most recent roll',
				hint: 'The most recent roll is automatically expanded and second to last automatically collapsed. Without this setting, all rolls start collapsed.',
				scope: 'client',
				type: Boolean,
				default: true,
				config: true,
				onChange: setting => {
					this._settings.newRollExpanded = setting;
				}
			}
		};
	}

   	registerSettings(){
   		Object.keys(this.SETTINGS).forEach(setting => {
   			game.settings.register('separate-roll-log', setting, this.SETTINGS[setting]);
   		})
   		console.log("Separate Roll Log | Settings registered");
   	}

   	loadSettings(){
   		Object.keys(this.SETTINGS).forEach(setting => {
   			this._settings[setting] = game.settings.get('separate-roll-log', setting);
   		});
   		console.log("Separate Roll Log | Settings loaded", this._settings);
   	}

   	applySettings(){
   		this._setTimestampCSS();
   		if(this._settings.showSidebarRolls) this._rerenderSidebarRolls();
   		else this._hideSidebarRolls();
   		console.log("Separate Roll Log | Settings applied")
   	}

   	/* -------------------------------------------- */

  	/**
   	* Prepare the default data which is required to render 
   	*/
   	getData() {
	   	let data = {};
	   	
	   	data.rollMessages = game.messages.entities.filter(m => m.isRoll);

	   	data.filterArrUsers = [{name: 'All', val: 'all'}, 
			...game.users.entities
				.map(u => {return {name: u.name, val: u.id}})
				.sort((first,second) => { let a = first.name.toUpperCase(); let b = second.name.toUpperCase(); return a < b ? -1 : a > b ? 1 : 0; })
		];

		data.filterArrActors = [{name: 'All', val: 'all'}, 
			...game.users.entities.filter(u => !u.isGM && u.character)
				.map(u => {return {name: u.character.name, val: u.character.name}})
				.sort((first,second) => { let a = first.name.toUpperCase(); let b = second.name.toUpperCase(); return a < b ? -1 : a > b ? 1 : 0; }),
			{name: 'NPCs or non-actor rolls', val: 'npc'}
		];

		data.isGM = game.user.isGM;

	   	return data;
	}

   	/* -------------------------------------------- */

   	_removeDOMEl(mID){
   		let rollItem = $(`#separate-roll-log .roll[data-message-id="${mID}"`);
		if(rollItem.length) rollItem.slideUp(100, () => rollItem.remove());
   	}

   	/* Filter functions */
   	_applyFilters(){
   		
   		let userFilterVal = $('#separate-roll-log select[name="userFilter"]').val();
   		let userFilterText = $('#separate-roll-log select[name="userFilter"] option:selected').text();
   		let userFilterSet = userFilterVal !== 'all';
   		let actorFilterVal = $('#separate-roll-log select[name="actorFilter"]').val();
   		let actorFilterSetToPlayerName = !['all','npc'].includes(actorFilterVal);
   		let actorNPCFilter = actorFilterVal === 'npc';
   		let textFilterVal = $('#separate-roll-log input[name="textFilter"]').val();
   		let textFilterSet = textFilterVal && textFilterVal !== '';

   		let userNotFilterSelector = userFilterSet ? `[data-user-id="${userFilterVal}"]`: '' ;
   		let actorNotFilterSelector = actorFilterSetToPlayerName ? `[data-actor-alias="${actorFilterVal}"]` : '';
   		let textNotFilterSelector = textFilterSet ? `[data-filter-text*="${textFilterVal.toLowerCase()}"] `: '' ;
   		let notSelector = [userNotFilterSelector, actorNotFilterSelector, textNotFilterSelector].filter(s => s !== '').join(', ');

   		let actorPCFilterSelector = actorNPCFilter ? game.users.entities.filter(u => !u.isGM && u.character)
   			.map(u => `[data-actor-alias="${u.character.name}"]`).join(', ') : '';

   		let allRolls = $('#separate-roll-log .roll');
   		let rolls = allRolls;
   		if(notSelector) rolls = allRolls.not(notSelector);
   		if(actorPCFilterSelector) rolls = rolls.filter(actorPCFilterSelector);
   		
   		allRolls.show();
   		if(notSelector || actorPCFilterSelector) rolls.hide();

   		$('#separate-roll-log .title').text(`Showing ${!(notSelector || actorPCFilterSelector) ? 'all ':''}rolls ${
   			[userFilterSet ? 'by '+userFilterText : '',
   				actorFilterSetToPlayerName ? 'for '+actorFilterVal : actorNPCFilter ? 'for NPCs' : '',
				textFilterSet ? 'with "'+textFilterVal+'"' : ''].filter(s=>s!=='').join(' OR ')}`);

	}

	_setTimestampCSS(){
		let app = $('#separate-roll-log');
		switch(this._settings.timestampFormat){
			case 'dynamic':
				app.addClass('dynamic-timestamps').removeClass('short-timestamps');
				break;
			case 'short':
				app.addClass('short-timestamps').removeClass('dynamic-timestamps');
				break;
			case 'none':
				app.removeClass('dynamic-timestamps short-timestamps');
		}
	}

	_hideFormulasFromChatLog(){
		//Get all the roll DOM elements in the primary chat log and map that jQuery array to a jQuery array of actual ChatMessage objects
		$('#sidebar #chat .dice-formula, #sidebar #chat .dice-tooltip').parents('li.message').map( (i,m) => {
				return game.messages.entities.find(message => message.id === $(m).attr('data-message-id'))
			// Filter out any ChatMessages that they SHOULD be allowed to see (their own rolls or ones for an actor they have observer perms for)
			}).filter( (i,message) => { 
				return !(message.user.isSelf || (message.data.speaker && message.data.speaker.actor && game.actors.entities.find(a => a.id === message.data.speaker.actor).hasPerm(game.user, "OBSERVER"))) 
			// Map it back to jQuery obj of roll DOM elements and then remove the dice formulas/tooltips from sidebar chatlog
			}).map( (i,m) => {
				return $(`#sidebar #chat li.message[data-message-id="${m.id}"]`).find('.dice-formula, .dice-tooltip').remove() 
			});
	}

	_rerenderSidebarRolls(){
		game.messages.entities.filter(m => m.isRoll).forEach(async m => {
			let newHTML = await m.render();
			$(`#sidebar #chat-log [data-message-id="${m.id}"]`).replaceWith(newHTML);
			if(this._settings.hideFormulas)
				this._hideFormulasFromChatLog();
		});
	}

	_hideSidebarRolls(){
		$('#sidebar #chat-log .dice-roll').parents('.message').css('display','none').children('.message-content').children().remove();
	}

	_applySidebarRollsOne(message, data, html){
		// Now hide roll/formulas in the main chat as necessary:
    	if(!this._settings.showSidebarRolls){
    		$(html).css('display','none').children('.message-content').children().remove();
    	}
    	else if(this._settings.hideFormulas &&
    		!(message.user.isSelf || 
    			(message.data.speaker && message.data.speaker.actor && 
    				game.actors.entities.find(a => a.id === message.data.speaker.actor).hasPerm(game.user, "OBSERVER"))) ){
    		html.find('.dice-formula, .dice-tooltip').remove();
    	}
	}

  	/**
   	* Add event listeners to UI elements
   	*/
	activateListeners(html) {
		html.find('.roll').find('.diceresult .dice').parents('.expandable.roll').click(function(){$(this).toggleClass('expand')})

		html.find('.expand-btn, .title').click(function(){
			let appEl = $(this).parent('#separate-roll-log');
			appEl.toggleClass('show-settings');
			// appEl.children('.roll-log-settings').slideToggle(100);
		});

		html.find('select[name="userFilter"], select[name="actorFilter"], input[name="textFilter"]').change(()=>this._applyFilters());

		html.find('.message-delete').click(function(ev){
			ev.stopPropagation();
			let el = $(this);
			let parent = el.parents('.roll');
			let mID = parent.attr('data-message-id');
			let message = game.messages.get(mID);
			ui.separateRollLog._removeDOMEl(mID);
			message.delete();
		})
	}
}

Handlebars.registerHelper('sepRollLogIsExpandable', function(message, options){
	return (!ui.separateRollLog._settings.hideFormulas && message.visible) ||
	 (game.user.isGM || message.user.isSelf || 
	 	(message.data.speaker && message.data.speaker.actor && 
	 		game.actors.entities.find(a => a.id === message.data.speaker.actor).hasPerm(game.user, "OBSERVER")));
});

Handlebars.registerHelper('sepRollLogTimestampDynamic', function(timestamp, options) {
	let date = new Date(timestamp)
	let today = new Date();
	if(date.toLocaleDateString() === today.toLocaleDateString())
  		return date.toLocaleString('en-US', {hour: '2-digit', minute: '2-digit'});
  	else
  		return date.toLocaleString('en-US', {dateStyle: 'short', timeStyle: 'short'});
});
Handlebars.registerHelper('sepRollLogTimestampShort', function(timestamp, options) {
	return new Date(timestamp).toLocaleString('en-US', {hour: '2-digit', minute: '2-digit'});
});


Handlebars.registerHelper('sepRollLogGetRollName', function(message, options) {
	if(message.data.speaker.alias && message.visible){
		return message.data.speaker.alias;
	}
	else{
		return message.user.name;
	}
});

Handlebars.registerHelper('sepRollLogProcessRollPart', function(part, object) {
	let html = "";
	switch (typeof part){
		case 'object':
			let facesClass = [4,6,8,10,12,20].includes(part.faces) ? part.faces : 6;
			html+=`<span>[${part.formula}]</span>`;
			for(let rollObj of part.rolls){
				html+=`<span class="die d${facesClass}${(rollObj.roll === part.faces ? ' max' : rollObj.roll === 1 ? ' min' : '')}${rollObj.discarded ? ' discarded' : ''}" title="d${part.faces} = ${rollObj.roll}">${rollObj.roll}</span>`;
			}
		break;
		case 'string':
			html+=`<span>${part}</span>`;
	}
	return new Handlebars.SafeString(html);
});

Handlebars.registerHelper('sepRollLogCheckForMinMax', function(roll, object) {
	let isNat20 = false;
	let isNat1 = false;
	for(let part of roll.parts){
		if(typeof part === 'object' && part.faces === 20 && part.rolls.find(r => r.roll === 20 && !r.discarded))
			isNat20 = true;
		if(typeof part === 'object' && part.faces === 20 && part.rolls.find(r => r.roll === 1 && !r.discarded))
			isNat1 = true;
	}
	return isNat20 ? 'max' : isNat1 ? 'min' : '';
});

Handlebars.registerHelper('toLowerCase', function(str, object) {
	return str ? str.toLowerCase() : '';
});

// Hooks I want to setup right away (not on render)

Hooks.on('init', () => {
	ui.separateRollLog = new SeparateRollLog();
	ui.separateRollLog.registerSettings();
	ui.separateRollLog.loadSettings();
});

Hooks.on('ready',() => {
	ui.separateRollLog.render(true);
	ui.separateRollLog.applySettings();
});

Hooks.on('renderSeparateRollLog', () => {
	let $container = $('#separate-roll-log .roll-content');
	$container.animate({scrollTop: $container.prop('scrollHeight')},100);
});
