# Separate Roll Log
Separate Roll Log is an FVTT module that creates an always-visible small app window near the sidebar that contains all the roll logs from the chat.

## Goals of the module:
*  Clean up the chat log in sidebar so that actual chat is easier to read amonst many dice rolls.
*  Condense the roll log itself so that it's easy to quickly look through old rolls.
*  Provide filters to allow users to find older rolls more easily.

### Feature list:
*   Filter by user, actor alias and free-text
*   Settings to customize intrusion on the primary chat log, from untouched to completely hiding all roll elements from it.
*   Setting to hide dice formulas from users if they don't have proper permissions over the actor involved. Allows GM to still show players the roll result without revealing the whole formula. (i.e. prevent giving away a creature's high/low save or skill check on the first single roll)

## Changelog
*  0.3.2 - Initial posting on gitlab. Support tested in pf2e system.